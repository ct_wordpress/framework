<?php

namespace Codetikkers\Wordpress\Translation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;

class TranslationServiceProvider extends ServiceProvider {

	protected $defer = true;

	public function register()
	{
		$this->app->singleton('translation.loader', function($app) {
			return new FileLoader($app['files'], $app['path.lang']);
		});

		$this->app->singleton('translator', function($app) {
			$loader = $app['translation.loader'];

			$trans = new Translator($loader, 'nl');
			$trans->setFallback('en');

			return $trans;
		});
	}

	public function provides()
	{
		return ['translator', 'translation.loader'];
	}
}