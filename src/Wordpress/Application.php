<?php namespace Codetikkers\Wordpress;

use Codetikkers\Wordpress\Acf\AcfServiceProvider;
use Codetikkers\Wordpress\Admin\AdminServiceProvider;
use Codetikkers\Wordpress\Config\ConfigServiceProvider;
use Codetikkers\Wordpress\Filesystem\FilesystemServiceProvider;
use Codetikkers\Wordpress\Request\RequestServiceProvider;
use Codetikkers\Wordpress\Translation\TranslationServiceProvider;
use Codetikkers\Wordpress\Validation\ValidationServiceProvider;
use Codetikkers\Wordpress\View\ViewServiceProvider;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class Application
 *
 * @package Codetikkers\Wordpress
 */
class Application extends Container {

	protected $paths = [];

	protected $loadedProviders = [];

	/**
	 * Application constructor.
	 */
	public function __construct(array $paths)
	{
		$this->registerBaseBindings();
		$this->registerBasePaths($paths);

		$this->app->bind('events', function ($container) {
			return new Dispatcher($container);
		});

		$this->register(RequestServiceProvider::class);
		$this->register(FilesystemServiceProvider::class);
		$this->register(ViewServiceProvider::class);
		$this->register(ConfigServiceProvider::class);
		$this->register(TranslationServiceProvider::class);
		$this->register( ValidationServiceProvider::class);
		$this->register(AcfServiceProvider::class);
		$this->register(AdminServiceProvider::class);

		$session  = new Session();
		$session->start();
		app('request')->setSession($session);

		$this->instance(
			'path.themes.current',
			$paths['base'] . '/public/themes/' . config('theme.name')
		);
	}

	/**
	 *
	 */
	protected function registerBaseBindings()
	{
		static::setInstance($this);
		$this->instance('app', $this);
	}

	/**
	 * @param array $paths
	 *
	 * @return $this
	 */
	protected function registerBasePaths(array $paths)
	{
		$this->paths = $paths;

		foreach ($paths as $key => $path)
		{
			$path =  ($key == 'base')
				? rtrim($path, '\/')
				: $paths['base'] . DS . $path;

			$this->instance('path.' . $key, $path);
		}
	}

	public function start()
	{
		$this['acf']->init();
		$this['wordpress.admin']->init();
		$this['wordpress.admin.dashboard']->init();

		Theme::init();
		//Blade::init();
		Mailer::init();
	}

	/**
	 * Return environment variable
	 *
	 * @return mixed
	 */
	public function environment()
	{
		return env('WP_ENV', 'production');
	}

	/**
	 * Register a service provider with the application.
	 *
	 * @param ServiceProvider|string $provider
	 */
	public function register($provider)
	{
		if ( ! $provider instanceof ServiceProvider) {
			$provider = new $provider($this);
		}
		if (array_key_exists($providerName = get_class($provider), $this->loadedProviders)) {
			return;
		}

		$this->loadedProviders[$providerName] = true;
		$provider->register();

		if (method_exists($provider, 'boot')) {
			$provider->boot();
		}
	}
}