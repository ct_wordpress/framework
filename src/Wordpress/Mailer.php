<?php namespace Codetikkers\Wordpress;

class Mailer {

	public static function init()
	{
		add_action('phpmailer_init', function($phpmailer)
		{
			if (config('mail.driver') == 'smtp')
			{
				$phpmailer->isSmtp();
				$phpmailer->Host = config('mail.host');
				$phpmailer->Port = config('mail.port');
				$phpmailer->SMTPAuth = config('mail.auth');
				$phpmailer->Username = config('mail.username');
				$phpmailer->Password = config('mail.password');
			}
		});
	}
}