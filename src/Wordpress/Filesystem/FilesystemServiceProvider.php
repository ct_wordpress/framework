<?php

namespace Codetikkers\Wordpress\Filesystem;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class FilesystemServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->singleton('files', function() {
			return new Filesystem();
		});
	}
}