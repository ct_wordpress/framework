<?php

namespace Codetikkers\Wordpress\Validation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Translation\FileLoader;
use Illuminate\Validation\Factory;

class ValidationServiceProvider extends ServiceProvider
{
	protected $defer = true;

	public function register() {

		$this->app->singleton('validator', function ($app) {
			return new Factory($app['translator'], $app);
		});
	}

	public function provides() {
		return ['validator'];
	}
}