<?php namespace Codetikkers\Wordpress\Admin;

class DashboardBuilder extends Dashboard {

	public function widget($id, $title, $content)
	{
		array_push($this->widgets, [
			'id' => $id,
			'title' => $title,
			'content' => $content,
		]);
	}
}