<?php namespace Codetikkers\Wordpress\Admin;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->singleton('wordpress.admin', function() {
			return new Admin;
		});
		$this->app->singleton('wordpress.admin.dashboard', function() {
			return new DashboardBuilder();
		});
	}

	public function boot()
	{
		app('wordpress.admin.dashboard')->widget('ct_dashboard_support', 'Codetikker nodig?', function($post) {
			echo "Heb je een vraag of een opmerking, stuur dan een mail naar <a href='mailto:wordpress@codetikkers.nl'>wordpress@codetikkers.nl</a>, bel naar 0118 - 79 45 11 of kom gezellig langs.";
		});
	}
}