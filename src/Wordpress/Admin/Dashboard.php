<?php namespace Codetikkers\Wordpress\Admin;

class Dashboard {

	protected $widgets = [];

	public function init() {
		$this->registerActions();
	}

	public function registerActions()
	{
		add_action('wp_dashboard_setup', array($this, 'addWidgets'));
	}

	public function addWidgets()
	{
		foreach ($this->widgets as $widget)
		{
			wp_add_dashboard_widget($widget['id'], $widget['title'], $widget['content']);
		}
	}
}