<?php namespace Codetikkers\Wordpress\Request;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Concerns\InteractsWithContentTypes;
use Illuminate\Http\Concerns\InteractsWithFlashData;
use Illuminate\Http\Concerns\InteractsWithInput;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Illuminate\Support\Arr;
use Illuminate\Http\Request as LaravelRequest;

/**
 * Class Request
 *
 * @package Codetikkers\Wordpress\Request
 */
class Request extends SymfonyRequest
{
	use InteractsWithInput,
		InteractsWithContentTypes;

	/**
	 * @var null
	 */
	protected $session = null;

	/**
	 * All of the converted files for the request.
	 *
	 * @var array
	 */
	protected $convertedFiles;

	public static function capture() {
		return static::createFromGlobals();
	}

	/*
	 |------------------------------------------------------------------------------------------------------------------
	 | Wordpress
	 |------------------------------------------------------------------------------------------------------------------
	 */

	public function verify_nonce($action = -1, $token = '_wpnonce') {
		$token = $this->post($token);

		return wp_verify_nonce($token, $action);
	}

	/*
	 |------------------------------------------------------------------------------------------------------------------
	 | Sessies
	 |------------------------------------------------------------------------------------------------------------------
	 */

	/**
	 * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
	 */
	public function setSession($session) {
		$this->session = $session;
	}

	/**
	 *
	 */
	public function flashInput() {
		$this->session->getFlashbag()->set("input", $this->input());
	}

	/**
	 * @param $key
	 *
	 * @return mixed
	 */
	public function flash($key) {

		if (is_array($key)) {
			return $this->session->getFlashbag()->setAll($key);
		}

		return $this->session->getFlashbag()->get($key);
	}

	/**
	 * @param null $key
	 *
	 * @return \Illuminate\Support\Collection|mixed
	 */
	public function old($key = null, $default = null) {
		$this->flashInput();
		$input = collect($this->session->getFlashbag()->get("input"));

		if (is_null($key)) {
			return $input;
		}

		return $input->get($key, $default);
	}

	/**
	 * Get the input source for the request.
	 *
	 * @return \Symfony\Component\HttpFoundation\ParameterBag
	 */
	protected function getInputSource() {
		if ($this->isJson()) {
			return $this->json();
		}

		return in_array($this->getRealMethod(), ['GET', 'HEAD']) ? $this->query : $this->request;
	}

	/*
	 |------------------------------------------------------------------------------------------------------------------
	 | Array magic
	 |------------------------------------------------------------------------------------------------------------------
	 */

	/**
	 * Get all of the input and files for the request.
	 *
	 * @return array
	 */
	public function toArray() {
		return $this->all();
	}

	/**
	 * Determine if the given offset exists.
	 *
	 * @param string $offset
	 * @return bool
	 */
	public function offsetExists($offset) {
		return Arr::has(
			$this->all() + $this->route()->parameters(),
			$offset
		);
	}

	/**
	 * Get the value at the given offset.
	 *
	 * @param string $offset
	 * @return mixed
	 */
	public function offsetGet($offset) {
		return $this->__get($offset);
	}

	/**
	 * Set the value at the given offset.
	 *
	 * @param string $offset
	 * @param mixed  $value
	 * @return void
	 */
	public function offsetSet($offset, $value) {
		$this->getInputSource()->set($offset, $value);
	}

	/**
	 * Remove the value at the given offset.
	 *
	 * @param string $offset
	 * @return void
	 */
	public function offsetUnset($offset) {
		$this->getInputSource()->remove($offset);
	}
}