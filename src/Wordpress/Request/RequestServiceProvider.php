<?php namespace Codetikkers\Wordpress\Request;

use Illuminate\Support\ServiceProvider;

class RequestServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->singleton('request', function() {
			return Request::capture();
		});
	}
}