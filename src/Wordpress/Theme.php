<?php namespace Codetikkers\Wordpress;

class Theme {

	public static function init()
	{
		static::head();

		add_filter('jpeg_quality', function ()
		{
			return config('wordpress.jpeg_quality');
		});

		/*
		 * Remove Microsoft Word formatting for TinyMCE.
		 */
		add_filter('content_save_pre', function ($content)
		{
			return preg_replace('/<!--\[if gte mso.*?-->/ms', '', $content);
		});

		/*
		 * Remove special characters in file names.
		 */
		add_filter('sanitize_file_name', function ($name)
		{
			return remove_accents($name);
		}, 10, 2);

		/**
		 * After Wordpress finished loading but before headers are sent.
		 */
		add_action('init', function ()
		{
			// Remove Emoji JS & CSS from header
			remove_action('wp_head', 'print_emoji_detection_script', 7);
			remove_action('wp_print_styles', 'print_emoji_styles');

			// Remove feeds from head
			remove_theme_support('automatic-feed-links');
			remove_action('wp_head', 'feed_links_extra', 3);
		});

		/**
		 * Upon echo of footer
		 */
		add_action('wp_footer', function ()
		{
			wp_deregister_script('wp-embed');
		});

		add_action('after_setup_theme', function ()
		{
			static::supports();
			static::navigation();
		});

		add_filter('mod_rewrite_rules', function ($rules)
		{
			return static::secureWpConfig() . $rules;
		});

		add_action('wp_before_admin_bar_render', function ()
		{
			static::editAdminBar();
		});
	}

	protected static function editAdminBar()
	{
		global $wp_admin_bar;

		/* Remove their stuff */
		$wp_admin_bar->remove_menu('wp-logo');
		$wp_admin_bar->remove_node('customize');

		if (config('theme.comments', false))
		{
			$wp_admin_bar->remove_menu('comments');
		}
	}

	protected static function secureWpConfig()
	{
		$r = "# Protect wp-config.php \n";
		$r .= "<Files wp-config.php>\n";
		$r .= "  Order Allow,Deny\n";
		$r .= "  Deny from all\n";
		$r .= "</Files>\n\n";

		return $r;
	}

	protected static function head()
	{
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');

		/* Remove Wordpress generator meta key */
		if ( ! config('wordpress.generator'))
		{
			remove_action('wp_head', 'wp_generator');
		}
	}

	protected static function supports()
	{
		add_theme_support('html5', config('theme.supports.html5', false));
		if (config('theme.supports.title-tag'))
		{
			add_theme_support('title-tag');
		}
		if (config('thumbnail.featured'))
		{
			add_theme_support('post-thumbnails');
		}

		foreach (config('thumbnail.sizes', []) as $key => $size)
		{
			list($width, $height, $crop) = $size;
			add_image_size($key, $width, $height, $crop);
		}
	}

	protected static function navigation()
	{
		register_nav_menus(config('navigation.menus'));
	}
}