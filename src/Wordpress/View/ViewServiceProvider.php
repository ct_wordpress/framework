<?php namespace Codetikkers\Wordpress\View;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;

class ViewServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->registerEngineResolver();
		$this->registerViewFactory();
	}

	protected function registerEngineResolver()
	{
		$serviceprovider = $this;

		$this->app->singleton('view.engine.resolver', function () use ($serviceprovider) {
			$resolver = new EngineResolver();

			foreach (['php', 'blade'] as $engine) {
				$serviceprovider->{'register' . ucfirst($engine) . 'Engine'}($engine, $resolver);
			}

			return $resolver;
		});
	}

	protected function registerPhpEngine($engine, EngineResolver $resolver)
	{
		$resolver->register($engine, function () {
			return new PhpEngine();
		});
	}

	protected function registerBladeEngine($engine, EngineResolver $resolver)
	{
		$container = $this->app;

		$storage    = $container['path.storage'] . '/cache/views';
		$filesystem = $container['files'];

		$bladeCompiler = new BladeCompiler($filesystem, $storage);
		$this->app->instance('blade', $bladeCompiler);

		$resolver->register($engine, function () use ($bladeCompiler) {
			return new CompilerEngine($bladeCompiler);
		});
	}

	protected function registerViewFactory()
	{
		// Register the View Finder first.
		$this->app->singleton('view.finder', function ($container) {
			return new ViewFinder($container['files'], [], ['blade.php', 'scout.php', 'twig', 'php']);
		});

		$this->app->singleton('view', function ($container) {
			$factory = new Factory($container['view.engine.resolver'], $container['view.finder'], $container['events']);
			// Set the container.
			$factory->setContainer($container);

			// We will also set the container instance on this view environment since the
			// view composers may be classes registered in the container, which allows
			// for great testable, flexible composers for the application developer.
			$factory->setContainer($container);
			$factory->share('app', $container);

			return $factory;
		});
	}

	public function boot()
	{
		$container = $this->app;

		$container['view.finder']->addLocation($container['path.resources'] . '/views');
	}
}