<?php namespace Codetikkers\Wordpress\Finder;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class FinderServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->bind('filesystem', function() {
			return new Filesystem();
		});
	}
}