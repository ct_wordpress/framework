<?php namespace Codetikkers\Wordpress;

use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Bootstrap {

	/**
	 * @var array
	 */
	protected $paths;

	/**
	 * Bootstrap constructor.
	 *
	 * @param array $paths
	 */
	public function __construct(array $paths)
	{
		$this->paths = $paths;

		$this->loadEnvironment();
		$this->loadErrorHandler();
	}

	/**
	 * Load .env variables
	 */
	protected function loadEnvironment()
	{
		$dotenv = new \Dotenv\Dotenv($this->paths['base']);

		if (file_exists($this->paths['base'] . '/.env')) {
			$dotenv->load();
			$dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_HOME']);
		}
	}

	/**
	 *
	 */
	protected function loadErrorHandler()
	{
		if (env('WP_ENV') !== 'production') {
			$whoops = new Run();
			$whoops->pushHandler(new PrettyPageHandler());
			$whoops->register();
		}
	}
}