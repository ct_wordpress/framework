<?php namespace Codetikkers\Wordpress\Acf;

class AcfBuilder {

	protected $canWriteJson = false;

	public function __construct()
	{
		$this->canWriteJson = (is_writable(app('path.themes.current') . "/acf-json"));
	}

	public function init()
	{
		if (class_exists('acf'))
		{
			$this->registerActions();
			$this->registerOptionPages();
			$this->protectAcfAdmin();
		}
	}

	protected function registerActions()
	{
		if (!$this->canWriteJson)
		{
			add_action('admin_notices', function ()
			{
				echo '<div class="error notice">';
				echo '<p>Please ensure the ACF-JSON directory is writable!</p>';
				echo '</div>';
			});
		}
	}

	protected function registerOptionPages()
	{
		// alleen laden in admin
		if (!is_admin()) return true;

		// pro versie is geladen
		if (function_exists('acf_add_options_page'))
		{
			foreach (config('acf.options.pages') as $page)
			{
				acf_add_options_page($page);
			}

			foreach (config('acf.options.subpages') as $subpage)
			{
				acf_add_options_sub_page($subpage);
			}
		}
	}


	protected function protectAcfAdmin()
	{
		// alleen laden in admin
		if (!is_admin()) return true;

		if (!in_array(get_current_user_id(), config('acf.show_admin', [1])))
		{
			add_filter('acf/settings/show_admin', '__return_false');
		}
	}
}