<?php namespace Codetikkers\Wordpress\Acf;

use Illuminate\Support\ServiceProvider;

class AcfServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->singleton('acf', function () {
			return new AcfBuilder();
		});
	}

	public function boot()
	{
//		add_action('init', function() {
//
//			dd(class_exists('asf'));
//
//		});
	}
}