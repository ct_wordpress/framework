<?php namespace Codetikkers\Wordpress\Config;

use Illuminate\Config\Repository;
use Symfony\Component\Finder\Finder;

class Config extends Repository {

	public function loadFiles($path, $environment = null)
	{
		$this->configPath = $path;

		foreach ($this->getConfigFiles() as $key => $path)
		{
			$this->set($key, require $path);
		}

		foreach ($this->getConfigFiles($environment) as $key => $path)
		{
			$envConfig = require $path;

			foreach ($envConfig as $envKey => $value)
			{
				$this->set($key . "." . $envKey, $value);
			}
		}
	}

	protected function getConfigFiles($environment = null)
	{
		$path = $this->configPath;

		if ($environment)
		{
			$path .= DIRECTORY_SEPARATOR . $environment;
		}

		if (!is_dir($path))
		{
			return [];
		}

		$files    = [];
		$phpFiles = Finder::create()->files()->name('*.php')->in($path)->depth(0);

		foreach ($phpFiles as $file)
		{
			$files[basename($file->getRealPath(), '.php')] = $file->getRealPath();
		}

		return $files;
	}
}