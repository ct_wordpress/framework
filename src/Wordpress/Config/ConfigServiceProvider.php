<?php namespace Codetikkers\Wordpress\Config;

use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->singleton('config', function() {
			return new Config();
		});
	}

	public function boot()
	{
		app('config')->loadFiles(
			app('path.config'),
			app()->environment()
		);

		with(new WordpressConstants())->setConstants();
	}
}