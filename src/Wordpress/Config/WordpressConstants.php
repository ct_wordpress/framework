<?php namespace Codetikkers\Wordpress\Config;

class WordpressConstants {

	/**
	 * Define Wordpress constants
	 */
	public function setConstants()
	{
		define('WP_ENV', app()->environment());

		$this->setAuthentication();
		$this->setDatabase();

		/**
		 * Paths & Urls
		 */
		define('WP_HOME', env('WP_HOME'));
		define('WP_SITEURL', env('WP_SITEURL', WP_HOME . "/wordpress"));

		/* Custom content directories */
		define('WP_CONTENT_DIR', env('WP_CONTENT_DIR', app('path.public')));
		define('WP_CONTENT_URL', env('WP_CONTENT_URL', WP_HOME));

		/**
		 * Debugging
		 */
		define('WP_DEBUG', config('error.debug'));
		define('WP_DEBUG_DISPLAY', config('error.debug'));
		define('SCRIPT_DEBUG', config('error.debug'));

		/**
		 * Wordpress
		 */
		/* Set default language */
		define('WPLANG', env('WP_LANG', 'nl_NL'));
		define('WP_LANG_DIR', app('path.lang'));

		define('DISALLOW_FILE_EDIT', config('wordpress.disallow_file_edit'));
		define('IMAGE_EDIT_OVERWRITE', config('wordpress.image_edit_overwrite'));
		define('WP_POST_REVISIONS', config('wordpress.wp_post_revisions'));
		define('EMPTY_TRASH_DAYS', config('wordpress.empty_trash_days'));

		define( 'WP_DEFAULT_THEME', config('theme.name'));
	}

	/**
	 * Define authentication unique keys and salts
	 */
	protected function setAuthentication()
	{
		/**
		 * Authentication Unique Keys and Salts.
		 */
		define('AUTH_KEY', env('AUTH_KEY'));
		define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
		define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
		define('NONCE_KEY', env('NONCE_KEY'));
		define('AUTH_SALT', env('AUTH_SALT'));
		define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
		define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
		define('NONCE_SALT', env('NONCE_SALT'));
	}

	/**
	 * Define database settings
	 */
	protected function setDatabase()
	{
		global $table_prefix;

		/**
		 * Database
		 */
		define('DB_HOST', config('database.host') . ":" . config('database.port'));
		define('DB_NAME', config('database.name'));
		define('DB_USER', config('database.user'));
		define('DB_PASSWORD', config('database.pass'));
		define('DB_CHARSET', config('database.charset'));
		define('DB_COLLATE', config('database.collate'));

		$table_prefix = config('database.prefix');
	}

}