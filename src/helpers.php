<?php

use Illuminate\Container\Container;
use Illuminate\Support\Str;

if ( ! function_exists( 'env' ) ) {
	/**
	 * Gets the value of an environment variable.
	 * Supports boolean, empty and null.
	 *
	 * @param string $key
	 * @param mixed $default
	 *
	 * @return mixed
	 */
	function env( $key, $default = null ) {
		$value = getenv( $key );
		if ( $value === false ) {
			return value( $default );
		}
		switch ( strtolower( $value ) ) {
			case 'true':
			case '(true)':
				return true;
			case 'false':
			case '(false)':
				return false;
			case 'empty':
			case '(empty)':
				return '';
			case 'null':
			case '(null)':
				return;
		}
		if ( strlen( $value ) > 1 && Str::startsWith( $value, '"' ) && Str::endsWith( $value, '"' ) ) {
			return substr( $value, 1, - 1 );
		}

		return $value;
	}
}

if ( ! function_exists( 'app' ) ) {
	/**
	 * @return \Codetikkers\Wordpress\Application|null
	 */
	function app( $make = null, $parameters = [] ) {
		if ( is_null( $make ) ) {
			return Container::getInstance();
		}

		return Container::getInstance()->make( $make, $parameters );
	}
}

if ( ! function_exists( 'config' ) ) {
	/**
	 * @param string|array|null $key
	 * @param string|null $default
	 *
	 * @return \Illuminate\Config\string|array|null
	 */
	function config( $key = null, $default = null ) {
		if ( is_null( $key ) ) {
			return app( 'config' );
		}

		if ( is_array( $key ) ) {
			return app( 'config' )->set( $key );
		}

		return app( 'config' )->get( $key, $default );
	}
}

if ( ! function_exists('request')) {
	/**
	 * @return \Codetikkers\Wordpress\Request\Request
	 */
	function request($key = null, $default = null) {
		if (is_null($key)) {
			return app('request');
		}
		if (is_array($key)) {
			return app('request')->only($key);
		}
		$value = app('request')->__get($key);
		return is_null($value) ? value($default) : $value;
	}
}


if ( ! function_exists( 'asset' ) ) {
	function asset( $file ) {
		return get_option( 'home' ) . '/dist/' . $file;
	}
}

if ( ! function_exists( 'view' ) ) {
	/**
	 * Get the evaluated view contents for the given view.
	 *
	 * @param  string  $view
	 * @param  array   $data
	 * @param  array   $mergeData
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
	function view($view = null, $data = [], $mergeData = []) {
		$factory = app('view');

		if (func_num_args() === 0) {
			return $factory;
		}

		return $factory->make($view, $data)->render();
	}
}

if ( ! function_exists( 'dd' ) ) {
	function dd() {
		$args = func_get_args();
		call_user_func_array( 'dump', $args );
		die();
	}
}

if (!function_exists('validator')) {
	/**
	 * Create a new Validator instance.
	 *
	 * @param  array  $data
	 * @param  array  $rules
	 * @param  array  $messages
	 * @param  array  $customAttributes
	 * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory
	 */
	function validator(array $data = [], array $rules = [], array $messages = [], array $customAttributes = [])
	{
		$factory = app('validator');

		if (func_num_args() === 0) {
			return $factory;
		}

		return $factory->make($data, $rules, $messages, $customAttributes);
	}
}